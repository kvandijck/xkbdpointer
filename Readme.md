## Control your mouse using your keyboard

```xkbdptr``` allows you to control your mouse using your keyboard.

**Why is that usefull?**

I use it to control my homebrew HTPC using IR remote control.

Some X programs work fine using arrow keys.

But when you use a web browser, you mostly need some kind of mouse.
And I do have an air-mouse, but it is sometimes hard to keep the air-mouse
in position while clicking.

All this is much more stable if I use my arrow keys and enter button
from the IR remote control to control the mouse.

Since I use it only for some programs, ```xkbdptr``` can run a child program
and keeps doing its magic until the program dies.
