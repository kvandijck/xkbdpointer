PROGS	= xkbdptr
default	: $(PROGS)

PREFIX	= /usr/local

CC	= gcc
CFLAGS	= -Wall
CPPFLAGS= -D_GNU_SOURCE
LDLIBS	= -lXtst -lX11
INSTOPTS= -s

ifeq ($(VERSION),)
VERSION := $(shell git describe --tags --always)
endif

-include config.mk

CPPFLAGS += -DVERSION=\"$(VERSION)\"

xkbdptr:

install: $(PROGS)
	$(foreach PROG, $(PROGS), install -vp -m 0777 $(INSTOPTS) $(PROG) $(DESTDIR)$(PREFIX)/bin/$(PROG);)

clean:
	rm -rf $(wildcard *.o lib/*.o) $(PROGS)
