#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <poll.h>
#include <syslog.h>
#include <wait.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/extensions/XTest.h>

#define NAME "xkbdptr"
#ifndef VERSION
#define VERSION "?"
#endif

static const char help_msg[] =
	NAME ": control your mouse using the keyboard\n"
	"usage: " NAME " [COMMAND [ARGS]]\n"
	"     : " NAME " -?|-V\n"
	"\n"
	"Control your mouse using left, right, up, down, enter\n"
	"\n"
	"COMMAND [ARGS] is a program to run\n"
	NAME " runs as long as COMMAND runs\n"
	;

static int child;

static int max_loglevel = LOG_NOTICE;

__attribute__((format(printf,2,3)))
static void mylog(int level, const char *fmt, ...)
{
	va_list va;
	int ret;
	char *str;

	va_start(va, fmt);
	ret = vasprintf(&str, fmt, va);
	va_end(va);
	if (ret < 0)
		; /* nothing to print */
	else if (level <= max_loglevel)
		dprintf(STDERR_FILENO, "%s: %s\n", NAME, str);
	free(str);
	if (level < LOG_ERR) {
		if (child > 0)
			kill(child, SIGTERM);
		exit(EXIT_FAILURE);
	}
}

#define ESTR(x) strerror(x)

static Display *dpy;
static Window root;
static const char *dpyname;
static int xtstev, xtsterr, xtstmajor, xtstminor;

const char *const xevent_names[LASTEvent] = {
	[KeyPress] = "KeyPress",
	[KeyRelease] = "KeyRelease",
	[ButtonPress] = "ButtonPress",
	[ButtonRelease] = "ButtonRelease",
	[MotionNotify] = "MotionNotify",
	[KeymapNotify] = "KeymapNotify",
	[PropertyNotify] = "PropertyNotify",
	[ClientMessage] = "ClientMessage",
	[MappingNotify] = "MappingNotify",
	/* ... */
	//[GenericEvent] = "GenericEvent",
};

static int xerror(Display *dpy, XErrorEvent *ev)
{
	char xmsg[1024];
	char evid[16];

	sprintf(evid, "#%i", ev->type);
	//sprintf(xmsg, "error %i", ee->error_code);
	XGetErrorText(dpy, ev->error_code, xmsg, sizeof(xmsg));
	mylog(LOG_CRIT, "xlib error on %s for 0x%08lx: %s",
			xevent_names[ev->type] ?: evid,
			ev->resourceid, xmsg);
	return 0;
}

void xgrabkeycode(unsigned int keycode, unsigned int modifier)
{
	unsigned int modifiers[] = { 0, LockMask, };//, numlockmask, numlockmask|LockMask };
	int j, ret;

	for (j = 0; j < (int)(sizeof(modifiers)/sizeof(modifiers[0])); ++j) {
		ret = XGrabKey(dpy, keycode, modifier | modifiers[j], root,
				False, GrabModeAsync, GrabModeAsync);
		if (ret > 1)
			mylog(LOG_INFO, "XGrabKey %u: %i", keycode, ret);
	}
}

void xgrabkeysym(KeySym keysym, unsigned int modifier)
{
	KeyCode code;

	code = XKeysymToKeycode(dpy, keysym);
	if (!code)
		return;
	xgrabkeycode(code, modifier);
}

static double getnow(void)
{
	struct timespec ts;

	if (clock_gettime(CLOCK_MONOTONIC, &ts) < 0)
		mylog(LOG_ERR, "clock_gettime MONOTONIC: %s", ESTR(errno));
	return ts.tv_sec + ts.tv_nsec * 1e-9;
}

void xkeypressed(XEvent *e)
{
	static KeySym prevkey;
	static double prevnow;
	static int nequal;
	KeySym keysym;
	double now = getnow();
	int step;

	keysym = XLookupKeysym(&e->xkey, 0);

	if ((now - prevnow) > 0.5)
		/* reset acceleration */
		prevkey = 0;

	if (keysym == prevkey)
		++nequal;
	else
		nequal = 0;

	step = 5 + nequal;

	prevkey = keysym;
	prevnow = now;

	switch (keysym) {
	case XK_Left:
		XTestFakeRelativeMotionEvent(dpy, -step, 0, 0);
		break;
	case XK_Right:
		XTestFakeRelativeMotionEvent(dpy, +step, 0, 0);
		break;
	case XK_Up:
		XTestFakeRelativeMotionEvent(dpy, 0, -step, 0);
		break;
	case XK_Down:
		XTestFakeRelativeMotionEvent(dpy, 0, +step, 0);
		break;
	case XK_Return:
		XTestFakeButtonEvent(dpy, Button1, 1, 0);
		XTestFakeButtonEvent(dpy, Button1, 0, 10);
		break;
	default:
		printf("?"); fflush(stdout);
		break;
	}
}

static void sigchld(int sig)
{
	int ret, status;

	for (;;) {
		ret = waitpid(-1, &status, WNOHANG);
		if (ret == child) {
			/* forward exit status */
			if (WIFEXITED(status))
				exit(WEXITSTATUS(status));
			else if (WIFSIGNALED(status))
				exit(WTERMSIG(status) + 128);
			else
				exit(1);
		}
	}
}

int main(int argc, char *argv[])
{
	int ret;
	struct pollfd pfd;
	XEvent ev;

	if (argc == 2 && !strcmp(argv[1], "-?")) {
		fputs(help_msg, stdout);
		fflush(stdout);
		exit(0);
	} else if (argc == 2 && !strcmp(argv[1], "-V")) {
		printf("%s: %s, %s %s\n", NAME, VERSION, __DATE__, __TIME__);
		fflush(stdout);
		exit(0);
	}
	/* connect */
	XSetErrorHandler(xerror);
	if (!dpyname)
		dpyname = getenv("DISPLAY");
	dpy = XOpenDisplay(dpyname);
	if (!dpy)
		mylog(LOG_CRIT, "unable to open display '%s'", dpyname);
	root = RootWindow(dpy, 0);

	/* activate XTest */
	if (!XTestQueryExtension(dpy, &xtstev, &xtsterr, &xtstmajor, &xtstminor))
		mylog(LOG_CRIT, "XTest is not supported on '%s'", dpyname);

	/* setup */
	xgrabkeysym(XK_Left, 0);
	xgrabkeysym(XK_Right, 0);
	xgrabkeysym(XK_Up, 0);
	xgrabkeysym(XK_Down, 0);
	xgrabkeysym(XK_Return, 0);

	/* prepare main loop */
	pfd.fd = ConnectionNumber(dpy);
	pfd.events = POLLIN;

	if (argc > 1) {
		signal(SIGCHLD, sigchld);
		/* fork */
		child = ret = fork();
		if (ret < 0)
			mylog(LOG_CRIT, "fork failed: %s", ESTR(errno));
		if (!ret) {
			/* child */
			close(pfd.fd);
			execvp(argv[1], argv+1);
			mylog(LOG_CRIT, "execvp %s: %s", argv[1], ESTR(errno));
		}
	}

	for (;;) {
		XFlush(dpy);
		ret = poll(&pfd, 1, -1);
		if (ret < 0) {
			if (EINTR == errno)
				continue;
			mylog(LOG_CRIT, "poll: %s", ESTR(errno));
		}
		XEventsQueued(dpy, QueuedAfterReading);
		for (; XPending(dpy); ) {
			if (XNextEvent(dpy, &ev))
				mylog(LOG_CRIT, "XNextEvent: %s", ESTR(errno));

			switch (ev.type) {
			case KeyPress:
				xkeypressed(&ev);
				break;
			}
		}
	}
	return 0;
}
